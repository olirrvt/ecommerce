// Módulos Angular
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http'; 
import { ReactiveFormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';

// Componenetes Angular
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/Header/header/header.component';
import { HeaderLogadoComponent } from './components/Header/header-logado/header-logado.component';
import { FooterComponent } from './components/Footer/footer.component';
import { CarrinhoComponent } from './pages/carrinho/carrinho.component';
import { LoginComponent } from './pages/login/login.component';
import { CadastroComponent } from './pages/cadastro/cadastro.component';
import { HomeComponent } from './pages/home/home.component';
import { CadastrarProdutosComponent } from './pages/cadastrar-produtos/cadastrar-produtos.component';
import { VerProdutosComponent } from './pages/ver-produtos/ver-produtos.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HeaderLogadoComponent,
    FooterComponent,
    CarrinhoComponent,
    LoginComponent,
    CadastroComponent,
    HomeComponent,
    CadastrarProdutosComponent,
    VerProdutosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [ CookieService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
