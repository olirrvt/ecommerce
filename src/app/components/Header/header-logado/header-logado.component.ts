import { Component } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';
import { AuthService } from 'src/app/services/Auth/auth.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header-logado',
  templateUrl: './header-logado.component.html',
  styleUrls: ['./header-logado.component.css']
})
export class HeaderLogadoComponent {
  usuarioLogado: any;

  constructor(private auth: AuthService,
    private cookie: CookieService,
    private router: Router) {}

  ngOnInit(): void {
    this.pegarinfoDoUsuarioLogado();
  }

  pegarinfoDoUsuarioLogado() {
    this.auth.pegarInfoUsuario().subscribe(res => {
      this.usuarioLogado = res;
    });
  }

  deslogar(): void {
    this.cookie.delete('token');
    this.router.navigate(['/']);
  }

}
