import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Produto } from 'src/app/models/produto';
import { ProdutosService } from 'src/app/services/Produtos/produtos.service';

@Component({
  selector: 'app-cadastrar-produtos',
  templateUrl: './cadastrar-produtos.component.html',
  styleUrls: ['./cadastrar-produtos.component.css']
})
export class CadastrarProdutosComponent {
  formProduto!: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private produtoServices: ProdutosService) {}

  ngOnInit(): void {
    this.inicializarFormulario();
  }

  private inicializarFormulario(): void {
    this.formProduto = this.formBuilder.group({
      nome: ['', [Validators.required ] ],
      preco: ['', [Validators.required] ],
      categoria: ['', [Validators.required] ],
      promocao: [null],
    });
  }

  cadastrarPromocao(): void {
  const produto: Produto = this.formProduto.value;

    console.log(produto);

    this.produtoServices.cadastrarProduto(produto).subscribe(res => {
      console.log("criado!");
    });
  }

}
