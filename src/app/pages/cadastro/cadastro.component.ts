import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Usuario } from 'src/app/models/usuario';
import { CarrinhoService } from 'src/app/services/Carrinho/carrinho.service';
import { UsuariosService } from 'src/app/services/Usuarios/usuarios.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent {
  formCadastrar!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private usuarioServices: UsuariosService,
    private carrinhoServices: CarrinhoService) {}

  ngOnInit(): void {
    this.inicializarFormulario();
  }

  private inicializarFormulario(): void {
    this.formCadastrar = this.formBuilder.group({
      nome: ['', [Validators.required ] ],
      cpf: ['', [Validators.required] ],
      cep: ['', [Validators.required] ],
      email: ['', [Validators.required] ],
      senha: ['', [Validators.required] ],
      idade: ['', [Validators.required] ],
    });
  }

  cadastrarUsuario(): void {
    const usuario: Usuario = this.formCadastrar.value;
    this.usuarioServices.cadastrarUsuario(usuario).subscribe(res => {
      this.carrinhoServices.criarCarrinho(res.id);
    });
  }

}
