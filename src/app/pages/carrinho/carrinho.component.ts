import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/Auth/auth.service';
import { CarrinhoService } from 'src/app/services/Carrinho/carrinho.service';
import { ProdutoPromoService } from 'src/app/services/ProdutoPromocao/produto-promo.service';
import { ProdutosService } from 'src/app/services/Produtos/produtos.service';
import { PromocaoService } from 'src/app/services/Promocao/promocao.service';
import { UsuariosService } from 'src/app/services/Usuarios/usuarios.service';

@Component({
  selector: 'app-carrinho',
  templateUrl: './carrinho.component.html',
  styleUrls: ['./carrinho.component.css']
})
export class CarrinhoComponent implements OnInit {
  produtosDoUsuario: any[] = [];
  totalProdutos: number = 0;
  carrinho: any;
  itensCarrinho: any;
  idDoCarrinho: number = 0;
  quantidade: any[] = [];

  constructor(
    private auth: AuthService,
    private userServices: UsuariosService,
    private carrinhoServices: CarrinhoService,
    private produtosServices: ProdutosService,
    private produtoPromo: ProdutoPromoService,
    private promocaoServices: PromocaoService
  ) {}

  ngOnInit(): void {
    this.getUsuarioLogado();
  }

  getUsuarioLogado(): void {
    this.auth.pegarInfoUsuario().subscribe(res => {
      this.pegarCarrinhoUsuario(res.id);
    });
  }

  pegarCarrinhoUsuario(idUsuario: number): void {
    this.userServices.pegarCarrinhoPorIdUsuario(idUsuario).subscribe(carrinho => {
      this.carrinho = carrinho;
      this.idDoCarrinho = this.carrinho[0].id; 
      this.pegarItensCarrinho(this.idDoCarrinho);
    });
  }

  pegarItensCarrinho(idCarrinho: number): void {
    this.carrinhoServices.obterItensCarrinho(idCarrinho).subscribe(itens => {
      this.itensCarrinho = itens;

      for (const item of itens) {
        this.produtosServices.pegarProdutoId(item.produtoId).subscribe(produto => {
          this.produtosDoUsuario.push(produto);
          this.quantidade.push(item.quantidade);
          this.totalProdutos += item.quantidade;

          if (produto.id !== undefined) {
            this.obterPromocaoDoProduto(produto);
          }
        });
      }
    });
  }

  atualizarQuantidade(index: number, novaQuantidade: number) {
    this.totalProdutos -= this.quantidade[index];
    this.totalProdutos += novaQuantidade;

    this.quantidade[index] = novaQuantidade;
    this.calcularTotaisSemPromocao();
    this.calcularTotaisComPromocao();
  }

  obterPromocaoDoProduto(produto: any): void {
    this.promocaoServices.obterPromocaoPorId(produto.id).subscribe(promocoes => {
      produto.promocoes = promocoes;
      this.calcularTotaisComPromocao();
    });
  }

  calcularPrecoComPromocao(produto: any, quantidade: number): number {
    let precoTotal = produto.preco * quantidade;

    if (produto.promocoes && Array.isArray(produto.promocoes)) {
      for (const promocao of produto.promocoes) {
        switch (promocao.nomePromocao) {
          case 'leve2pague1':
            const quantidadePromo = Math.floor(quantidade / 2) + (quantidade % 2);
            precoTotal = Math.min(precoTotal, produto.preco * quantidadePromo);
            break;

          case '50% de desconto':
            precoTotal *= 0.5;
            break;

          case '3 por 10':
            const quantidadePromo3por10 = Math.floor(quantidade / 3) * 3;
            precoTotal = (quantidadePromo3por10 / 3) * 10 + (quantidade % 3) * produto.preco;
            break;

          // Adicione outros casos para outras promoções, se necessário

          default:
            break;
        }
      }
    }

    return precoTotal;
  }
  
  

  calcularTotaisSemPromocao(): number {
    let total = 0;
    for (let i = 0; i < this.produtosDoUsuario.length; i++) {
      total += this.produtosDoUsuario[i].preco * this.quantidade[i];
    }
    return total;
  }

  calcularTotaisComPromocao(): number {
    let total = 0;
    for (let i = 0; i < this.produtosDoUsuario.length; i++) {
      total += this.calcularPrecoComPromocao(this.produtosDoUsuario[i], this.quantidade[i]);
    }
    return total;
  }
}