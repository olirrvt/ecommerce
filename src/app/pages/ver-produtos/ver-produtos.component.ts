import { Component } from '@angular/core';
import { ItensCarrinho } from 'src/app/models/itens';
import { Produto } from 'src/app/models/produto';
import { AuthService } from 'src/app/services/Auth/auth.service';
import { CarrinhoService } from 'src/app/services/Carrinho/carrinho.service';
import { ProdutosService } from 'src/app/services/Produtos/produtos.service';
import { UsuariosService } from 'src/app/services/Usuarios/usuarios.service';

@Component({
  selector: 'app-ver-produtos',
  templateUrl: './ver-produtos.component.html',
  styleUrls: ['./ver-produtos.component.css']
})
export class VerProdutosComponent {
  listaProdutos: Produto[] = [];
  idUsuario: number = 0;
  idCarrinho: number = 0;
  carrinho: any;
  quantidade: number = 1;
  valorUnitario: number = 1;
  idPromocaoProduto: number = 0;

  constructor(
    private produtosServices: ProdutosService,
    private carrinhoServices: CarrinhoService,
    private userServices: UsuariosService,
    private auth: AuthService
  ) {}

  ngOnInit(): void {
    this.CarregarTodosProdutos();
    this.getUsuarioLogado();
  }

  getUsuarioLogado(): void {
    this.auth.pegarInfoUsuario().subscribe(res => {
      this.pegarCarrinhoUsuario(res.id);
    });
  }

  pegarCarrinhoUsuario(idUsuario: number): void {
    this.userServices.pegarCarrinhoPorIdUsuario(idUsuario).subscribe(carrinho => {
      this.carrinho = carrinho;
      this.idCarrinho = this.carrinho[0].id; 
    });
  }

  CarregarTodosProdutos() {
    this.produtosServices.pegarTodosProdutos().subscribe(produtos => {
      this.listaProdutos = produtos;
    });
  }

  adicionarItemAoCarrinho(idDoProduto: number): void {
    this.produtosServices.obterPromocaoDoProduto(idDoProduto).subscribe(res => {
      if(res[0]?.id !== undefined) {
        this.idPromocaoProduto = res[0]?.id;
      }

      const ItensCarrinho = {
        itensCarrinhosId: 0,
        carrinhoId: this.idCarrinho,
        produtoId: idDoProduto,
        quantidade: this.quantidade,
        valorUnitario: this.valorUnitario,
        promocaoAplicadaId: this.idPromocaoProduto
      };

      this.carrinhoServices.adicionarItem(ItensCarrinho).subscribe(res => {
        console.log("Inserido com sucesso!");
      });

    });
  }
}
