import { Component } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Usuario } from 'src/app/models/usuario';
import { AuthService } from 'src/app/services/Auth/auth.service';
import { Router } from '@angular/router';
import { Login } from 'src/app/models/login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  formLogin!: FormGroup;
  usuarioLogado: Usuario | undefined;

  constructor(
    private auth: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    private cookieService: CookieService) {
  }

  ngOnInit(): void {
    this.inicializarFormularioLogin();
  };

  private inicializarFormularioLogin(): void {
    this.formLogin = this.formBuilder.group({
      email: ['', Validators.required],
      senha: ['', Validators.required]
    });
  }

  efetuarLogin(): void {
    const login: Login = this.formLogin.value;

    this.auth.login(login).subscribe((res) => {

      if (res && res.token) {
        this.cookieService.set('token', res.token);
        this.router.navigate(['/']);
      } else {
        console.log("Erro ao efetuar o login");
      }
    });
    
  }

}
