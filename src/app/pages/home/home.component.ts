import { Component } from '@angular/core';
import { Produto } from 'src/app/models/produto';
import { ProdutosService } from 'src/app/services/Produtos/produtos.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  listaProdutos: Produto[] = [];

  constructor(
    private produtosServices: ProdutosService
  ) {}

  ngOnInit() {
    this.CarregarTodosProdutos();
  }

  CarregarTodosProdutos() {
    this.produtosServices.pegarTodosProdutos().subscribe(produtos => {
      this.listaProdutos = produtos;
    });
  }


}
