import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/Auth/auth.service';
import { CookieService } from 'ngx-cookie-service';
import { Usuario } from './models/usuario';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  dadosUsuarioLogado: Usuario | undefined;
  usuarioLogado = false;

  constructor(
    private auth: AuthService,
    private cookie: CookieService
  ) {}

  async ngOnInit(): Promise<void> {
    await this.carregarInformacoesUsuario();
    this.usuarioLogado = this.estaLogado();
  }

  async carregarInformacoesUsuario(): Promise<void> {
    this.dadosUsuarioLogado = await this.auth.pegarInfoUsuario().toPromise();
  }

  estaLogado(): boolean {
    return this.cookie.get('token') !== null;
  }
}