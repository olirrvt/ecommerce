import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from 'src/app/models/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  private url = "https://localhost:7289/api/Usuario/";

  constructor(private http: HttpClient) { }

  // método POST
  cadastrarUsuario(usuario: Usuario): Observable<any> {
    return this.http.post(this.url + "Cadastrar", usuario);
  }

  // GET por ID
  pegarCarrinhoPorIdUsuario(usuarioId: number): Observable<Usuario> {
    return this.http.get<Usuario>(this.url + "PegarIdCarrinho/" + usuarioId);
  }

  // PUT por ID
  alterarInfoUsuarios(usuario: Usuario): Observable<any> {
    return this.http.put(this.url + "Editar/" + usuario.id, usuario);
  }

  // DELETE por ID
  deletarUsuario(usuario: Usuario): Observable<any> {
    return this.http.put(this.url + "Deletar/" + usuario.id, usuario);
  }

}
