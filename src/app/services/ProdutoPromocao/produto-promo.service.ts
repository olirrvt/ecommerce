import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProdutosPromocao } from 'src/app/models/produtoPromocao';

@Injectable({
  providedIn: 'root'
})
export class ProdutoPromoService {
  private url = 'https://localhost:7289/api/ProdutosPromocoes';

  constructor(private http: HttpClient) { }

  associarProdutoPromocao(associacao: ProdutosPromocao): Observable<any> {
    return this.http.post<any>(`${this.url}/Associar`, associacao);
  }

  obterAssociacoesProdutoPromocao(produtoId: number): Observable<ProdutosPromocao[]> {
    return this.http.get<ProdutosPromocao[]>(`${this.url}/AssociacoesProdutoPromocao/${produtoId}`);
  }  

}
