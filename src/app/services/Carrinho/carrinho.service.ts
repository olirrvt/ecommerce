import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Carrinho } from 'src/app/models/carrinho';
import { ItensCarrinho } from 'src/app/models/itens';

@Injectable({
  providedIn: 'root'
})
export class CarrinhoService {

  constructor(private http: HttpClient) { }

  private url = "https://localhost:7289/api/carrinho/";

  criarCarrinho(usuarioId: number): Observable<any> {
    return this.http.post<Carrinho>(`${this.url}CriarCarrinho/${usuarioId}`, null);
  }

  adicionarItem(item: ItensCarrinho): Observable<any> {
    return this.http.post(`${this.url}AdicionarItem`, item);
  }

  obterItensCarrinho(carrinhoId: number): Observable<any[]> {
    return this.http.get<any[]>(`${this.url}Itens/${carrinhoId}`);
  }

  removerItemCarrinho(itemId: number): Observable<any> {
    return this.http.delete(`${this.url}RemoverItem/${itemId}`);
  }

}