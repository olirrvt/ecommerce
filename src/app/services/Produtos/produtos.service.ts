import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Produto } from 'src/app/models/produto';
import { Promocao } from 'src/app/models/promocao';

@Injectable({
  providedIn: 'root'
})
export class ProdutosService {
  private url = "https://localhost:7289/api/Produto/";

  constructor(
    private http: HttpClient
    ) { }

    // Método POST
    cadastrarProduto(produto: Produto): Observable<any> {
      return this.http.post<Produto>(`${this.url}/Cadastrar`, produto);
    }

    // Método GET
    pegarTodosProdutos(): Observable<Produto[]> {
      return this.http.get<Produto[]>(`${this.url}PegarTodos`)
    }

    // Método GET por ID
    pegarProdutoId(produtoId: number): Observable<Produto> {
      return this.http.get<Produto>(this.url + "PegarId/" + produtoId)
    }

    // Método para pegar uma promoção
    obterPromocaoDoProduto(idProduto: number): Observable<Promocao[]> {
      return this.http.get<Promocao[]>(`${this.url}${idProduto}/Promocoes`);
    }

    // Método PUT
    atualizarProduto(produto: Produto): Observable<any> {
      return this.http.put(this.url + "Editar/" + produto.id, produto);
    }

    // Método de Delete
    deletarProduto(produtoId: number): Observable<any> {
      return this.http.delete(this.url + "Deletar/" + {produtoId})
    }
    
}
