import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Promocao } from 'src/app/models/promocao';

@Injectable({
  providedIn: 'root'
})
export class PromocaoService {
  constructor(private http: HttpClient) { }

  private url = "https://localhost:7289/api/promocao/";

  obterTodasPromocoes(): Observable<Promocao[]> {
    return this.http.get<Promocao[]>(`${this.url}PegarTodas`);
  }

  obterPromocaoPorId(id: number): Observable<Promocao> {
    return this.http.get<Promocao>(`${this.url}PegarId/${id}`);
  }

  cadastrarPromocao(promocao: Promocao): Observable<Promocao> {
    return this.http.post<Promocao>(`${this.url}Cadastrar`, promocao);
  }

  editarPromocao(id: number, promocao: Promocao): Observable<Promocao> {
    return this.http.put<Promocao>(`${this.url}Editar/${id}`, promocao);
  }

  deletarPromocao(id: number): Observable<any> {
    return this.http.delete(`${this.url}Deletar/${id}`);
  }
}
