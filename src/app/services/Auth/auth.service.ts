import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { Login } from 'src/app/models/login';
import { Usuario } from 'src/app/models/usuario';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private url = "https://localhost:7289/api/Auth/";

  constructor( 
    private http: HttpClient,
    private cookie: CookieService ) { }

  // POST: Fazendo o Login do usuário

  login(login: Login): Observable<any> {
    return this.http.post<any>(this.url + "Login", login);
  }


  // GET: pegando info do user pelo token

  pegarInfoUsuario(): Observable<Usuario> {

    const token = this.cookie.get('token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    });

    return this.http.get<Usuario>(this.url + "Me", { headers });
  }

}
