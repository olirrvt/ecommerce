export class ItensCarrinho {
    itensCarrinhosId: number | undefined;
    carrinhoId: number | undefined;
    produtoId: number | undefined;
    quantidade: number | undefined;
    valorUnitario: number | undefined;
    promocaoAplicadaId: number | undefined;
}