import { ItensCarrinho } from "./itens";
import { ProdutosPromocao } from "./produtoPromocao";

export class Promocao {
    id: number = 0;
    nomePromocao: string = '';
    descricao: string = '';
    itensCarrinhos: ItensCarrinho[] = [];
    produtosPromocaos: ProdutosPromocao[] = [];
}