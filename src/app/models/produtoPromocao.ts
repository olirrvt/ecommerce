import { Produto } from "./produto";
import { Promocao } from "./promocao";

export class ProdutosPromocao {
    produtoPromocaoId: number | undefined;
    produtoId: number | undefined;
    promocaoId: number | undefined;
    produto: Produto | undefined;
    promocao: Promocao | undefined;
}