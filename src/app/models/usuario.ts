import { Carrinho } from "./carrinho";

export class Usuario {
    id: number = 0;
    nome: string = '';
    cpf: string = '';
    cep: string = '';
    email: string = '';
    senha: string = '';
    idade: number = 0;
    carrinhos: Carrinho[] = [];
}