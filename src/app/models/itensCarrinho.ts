import { Carrinho } from "./carrinho";
import { Produto } from "./produto";
import { Promocao } from "./promocao";

export interface ItensCarrinho {
    carrinhoId?: number;
    produtoId?: number;
    quantidade?: number;
    valorUnitario?: number;
    promocaoAplicadaId?: number;
    carrinho?: Carrinho;
    produto?: Produto;
    promocaoAplicada?: Promocao;
  }